#------------------------------------------------------------#
# Registers a scheduled task to run a PS script from local location
# Runs at any user logon
#------------------------------------------------------------#

# Variables
$triggers = @()
$taskname = 'Uncheck Compatibility View IE'
$taskdescription = 'Runs at logon. Unchecks IE Compatibility View settings to not show Intranet Sites in compatiblity view'
# Task Action
$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-executionpolicy bypass -File C:\windows\temp\Uncheck_CompatibilityView_IE.ps1'
# To trigger at Logon and only once
$Trigger1 = New-ScheduledTaskTrigger -AtLogOn

$triggers += $Trigger1

#Register Task now
Register-ScheduledTask -Action $action -Trigger $triggers -TaskName $taskname -Description $taskdescription -User "SYSTEM" -RunLevel Highest